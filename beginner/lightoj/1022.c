/*
* problem 1022 circle in square of lightoj
* ivanchoff@gmail.com
*/

#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define PI 2*acos(0.0)

int main(){
	double radio,area;
	int cases,i=0;
	scanf("%i\n",&cases);
	while(cases--){
		scanf("%lf\n",&radio);
		area = (2*radio)*(2*radio)-(PI*radio*radio);
		printf("Case %i: %.2lf\n",++i,area+10e-9);
	}
	return 0;
}
