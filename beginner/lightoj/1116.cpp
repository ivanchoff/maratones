/*
 * 1116.cpp - Discover the web
 * ivanchoff@gmail.com
 *
*/

#include<iostream>
 
 
using namespace std;
 
int main(){
		long long w;
		int n,m,tmp,j=0;
		int cases;
		cin>>cases;
		while(cases--){
				cin>>w;
				tmp=0;
				if(!(w & 1)){
						for(int i=0;i<63;i++){
								tmp=1<<i;
								if(w & tmp)break;
						}
						if((w/tmp)%2!=0){
								cout<<"Case "<<++j<<": "<<(w/tmp)<<" "<<tmp<<endl;
						}else cout<<"Case "<<++j<<": "<<"Impossible"<<endl;
 
				}else cout<<"Case "<<++j<<": "<<"Impossible"<<endl;
 
		}
		return 0;
}
