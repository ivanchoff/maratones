/*
 * 1045 - Digits of Factorial
 * ivanchoff@gmail.com
 *
 *
 */
#include <iostream>
#include <cmath>

using namespace std;
double fac[1000010];
int main(){
	int cases,num,base,res,j=0;
	cin>>cases;
	fac[0]=0;
	for(int i=1; i<1000010; i++) fac[i] = fac[i-1] + log(i);
	while(cases--){
		cin>>num>>base;
		res= (int)((fac[num] / log(base))+1);
		cout<<"Case "<<++j<<": " <<res<<endl;
	}
	return 0;
}
