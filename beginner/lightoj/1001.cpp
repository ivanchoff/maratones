/*
*   1001 - Opposite Task.
*
*   ivanchoff@gmail.com
*
*/

#include<iostream>

using namespace std;

int main(){
	int a,b;
	int i=-1;
	cin>>a;
	while(a>0){
		cin>>b;
		cout<<(b>>1)<<" "<<b-(b>>1)<<endl;
		a--;
	}
	return 0;
}
