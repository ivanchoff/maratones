#include <iostream>
#include <cstdio>
#include <cmath>

#define PI acos(-1)

using namespace std;

int main(){
	int cases,alfa,n,cont=0;
	float R;
	double r;

	cin>>cases;

	while(cases--){
		cin>>R>>n;
		alfa=PI/n;
		r=(R*atan(alfa))/(1 + atan(alfa));
		cout<<"R:"<<R<<"  n:"<<n<<"  r:"<<r<<endl;
		//printf("Case %i: %.9lf\n",++cont,r);
	}


	return 0;
}