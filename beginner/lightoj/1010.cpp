/*
 * 1010 -  Knights in Chessboard 
 * ivanchoff@gmail.com
 *
/

#include<iostream>

using namespace std;
int main(){
	int t,m,n,num=0;
	cin>>t;
	while(t--){
		cin>>m>>n;
		if( (m==1 || n==1) || ( m==2 && n==2 ) ) cout<<"Case "<<++num<<": "<<m*n<<endl;
		else if(m==2 || n==2){
			if( (((m*n)-2)%8)==0 )cout<<"Case "<<++num<<": "<<((m*n)/2)+1<<endl;
			else cout<<"Case "<<++num<<": "<<((m+n)/4)*4<<endl;
			
		}
		else cout<<"Case "<<++num<<": "<<((m*n+1)/2)<<endl;
	}

	return 0;
}
