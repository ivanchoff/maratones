/*
 * problem 1113-Discover the web  
 * ivanchoff@gmail.com
 * get AC
 */


#include<bits/stdc++.h>
#include<iostream>
#include<string>
#include<vector>
 
using namespace std;
int main(){
    vector<string> back;
    vector<string> forw;
    int cases,i=0;
    string cad,curl="http://www.lightoj.com/",tmp;
    cin>>cases;
    while(cases--){
        curl="http://www.lightoj.com/";
        back.clear();
        forw.clear();
        cin>>cad;
        cout<<"Case "<<++i<<":"<<endl;
        while(cad!="QUIT"){
            if(cad=="BACK"){
                if(!back.empty()){
                    forw.push_back(curl);
                    curl=back.back();
                    back.pop_back();
                    cout<<curl<<endl;
                }else cout<<"Ignored"<<endl;
            }
            if(cad=="FORWARD"){
                if(!forw.empty()){
                    back.push_back(curl);
                    curl=forw.back();
                    forw.pop_back();
                    cout<<curl<<endl;
                }else cout<<"Ignored"<<endl;
            }
            if(cad=="VISIT"){
                cin>>tmp;
                back.push_back(curl);
                curl=tmp;
                forw.clear();
                cout<<curl<<endl;
            }
            cin>>cad;
        }
    }
    return 0;
}
