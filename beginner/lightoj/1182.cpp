/*
 * 1182 - Parity - accepted
 * ivanchoff@gmail.com
 */

#include <bitset>
#include <iostream>
#include <climits>

using namespace std;

size_t popcount(size_t n) {
	std::bitset<sizeof(size_t) * CHAR_BIT> b(n);
	return b.count();
}

int main() {
	int cases,num,i=0,r=0;
	cin>>cases;  
	while(cases--){
		cin>>num;  
		r=popcount(num);
		(r%2==0)?cout<<"Case "<<++i<<": even"<<endl: cout<<"Case "<<++i<<": odd"<<endl;
	}
}
