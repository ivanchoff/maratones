/* 1189 - Sum of Factorials - accepted
 * ivanchoff@gmail.com
 *
 */
#include <bits/stdc++.h>

using namespace std;

long long fac[21];
int is[21];

void calcFact(){
	fac[0]=1;
	fac[1]=1;
	for(int i=2; i<21; i++)fac[i]=fac[i-1]*i;
}

int main (){
	int cases,j=0,plus=0;
	long long cont=0;
	long long n;
	calcFact();
	cin>>cases;
	while(cases--){
		cin>>n;
		memset(is,0,sizeof(is));
		plus=0;
		for(int i=20; i>=0; i--){
			if(fac[i]<=n){
				n-=fac[i];
				is[i]=1;
				plus++;
				
			}
			if(n==0)break;
		}
		cout<<"Case "<<++j<<": ";
		if(n==0){
			for(int i=0; i<21; i++){
				if(is[i]){
					cout<<i<<"!";
					if(plus>1){
						cout<<"+";
						plus--;
					}
				}
			}
			cout<<endl;
		}else{
			cout<<"impossible"<<endl;
		}
	}
	return 0;
}
