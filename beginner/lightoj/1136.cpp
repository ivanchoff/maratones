/*
 * ivanchoff - 1136 division by three - ligthoj - accepted
 */


#include<iostream>

using namespace std;


int main(){
	long long a,b,r;
	int cases,i=0;
	cin>>cases;
	while(cases--){
		cin>>a>>b;
		a--;
		r = ((2*b)/3) - ((2*a)/3);
		cout<<"Case "<<++i<<": "<<r<<endl;
	}
	return 0;
}
