/*
 * 1042 - secrets origin 
 * ivanchoff@gmail.com
 *
 */

#include<iostream>
using namespace std;
int main(){
    int cases,num,i=0,lowestbit,leftbit,rightbit,changedbit,res;
    cin>>cases;  
    while(cases--){
        cin>>num;  
        lowestbit = num & -num;
        leftbit = num + lowestbit;
        changedbit = num ^ leftbit;
        rightbit = (changedbit / lowestbit) >> 2;
        res = (leftbit | rightbit);
        cout<<"Case "<<++i<<": "<<res<<endl;
    }
    return 0;
}
