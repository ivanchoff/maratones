/*
* 1133 - Array Simulation
* ivanchoff@gmail.com
*
*/


#include<iostream>
#include<vector>
#include <algorithm>

using namespace std;
int main(){
		int cases,n,m,tmp,Z,Y,j=0;
		
		char cmd;

		cin>>cases;
		while(cases--){
				vector<int> array;
				cin>>n>>m;
				while(n--){
						cin>>tmp;
						array.push_back(tmp);
				}
				while(m--){
						cin>>cmd;
						if(cmd=='S'){
								cin>>tmp;
								for(int i=0; i<array.size();i++)array[i]+=tmp;
						}else if(cmd=='M'){
								cin>>tmp;
								for(int i=0; i<array.size();i++)array[i]*=tmp;
						}else if(cmd=='D'){
								cin>>tmp;
								for(int i=0; i<array.size();i++)array[i]/=tmp;
						}else if(cmd=='R'){
								reverse(array.begin(),array.end());
						}
						else if(cmd=='P'){
								cin>>Y>>Z;
								tmp=array[Y];
								array[Y]=array[Z];
								array[Z]=tmp;  
						}
				}
				cout<<"Case "<<++j<<":"<<endl;
				for(int i=0; i< array.size();i++)
						(i+1==array.size())? cout<<array[i]<<endl : cout<<array[i]<<" ";
				
		}

		return 0;
}
