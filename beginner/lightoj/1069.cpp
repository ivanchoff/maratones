/*
 * 1069 - lift
 * ivanchoff@gmail.com
 *
*/


#include<iostream>
#include<cstdlib>
 
 
using namespace std;
 
int main(){
		int cases,p,plift,time=0,i=0;
		cin>>cases;
		while(cases--){
				cin>>p>>plift;
				time=0;
				if(p!=plift) time= abs(p-plift)*4 + 4*p +9+ 10;
				else time= 4*p +9+ 10;
				cout<<"Case "<<++i<<": "<<time<<endl;
		}  
		return 0;
}
