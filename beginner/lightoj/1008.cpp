/*
* problem 1008 Fiebsieve's Fantabolous Birthday
* ivanchoff@gmail.com
*
*/

#include<iostream>
#include<cmath>
using namespace std;

long long s,n,x,y;
int main(){
	int cases,j=0;
	cin>>cases;
	while(cases--){
        cin>>s;
        n=ceil(sqrt(s));
        if(n*n-s<n)x=n,y=n*n-s+1;
        else x=-n*n+2*n+s-1,y=n;
        if(n&1)swap(x,y);
		cout<<"Case "<<++j<<": "<<x<<" "<<y<<endl;
    }
return 0;
}
