/*
 * 1107 - how cow
 * ivanchoff@gmail.com
 *
 */

#include<iostream>
     
using namespace std;
int main(){
  int cases,x1,x2,y1,y2,cx,cy,m,i=0;
  cin>>cases;
  while(cases--){
    cin>>x1>>y1>>x2>>y2;
    cin>>m;
    cout<<"Case "<<++i<<":"<<endl;
      while(m--){
        cin>>cx>>cy;
        (cx>x1 && cx<x2 && cy>y1 && cy<y2)? cout<<"Yes"<<endl : cout<<"No"<<endl;
      }
  }
  return 0;
}
