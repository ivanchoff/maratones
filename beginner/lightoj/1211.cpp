/*
 * 1211 - insertion cubes - accepted
 * ivanchoff@gmail.com
 */

#include<iostream>
using namespace std;

int main(){
	int cases,n,xi,yi,zi,xs,ys,zs,vol,i=0,x1,y1,z1,x2,y2,z2;
	cin>>cases;  
	while(cases--){
		cin>>n;  
		vol=0;
		cin>>xi>>yi>>zi>>xs>>ys>>zs;  
		n--;
		while(n--){
			cin>>x1>>y1>>z1>>x2>>y2>>z2;  
			if(x1>xi)xi=x1;
			if(y1>yi)yi=y1;
			if(z1>zi)zi=z1;  
			if(x2<xs)xs=x2;
			if(y2<ys)ys=y2;
			if(z2<zs)zs=z2;
		}
		if(xs-xi<0|| ys-yi <0 || zs-zi < 0 )vol=0;
		else vol=(xs-xi) * (ys-yi) * (zs-zi);
		cout<<"Case "<<++i<<": "<<vol<<endl;
	}
	return 0;
}
