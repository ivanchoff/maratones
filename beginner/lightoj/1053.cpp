/*
* 1053 - Higher Math 
* ivanchoff@gmail.com.
*
*/

#include<iostream>

using namespace std;

int main(){
	int cases,h,a,b,tmp,i=0;
	cin>>cases;

	while(cases--){
		cin>>h>>a>>b;
		if(a>h && a>b)tmp=a,a=h,h=tmp;
		else if(b>a && b>h)tmp=b,b=h,h=tmp;
		((h*h)==(a*a + b*b))? cout<<"Case "<<++i<<": yes"<<endl : cout<<"Case "<<++i<<": no"<<endl;
	}

	return 0;
}
