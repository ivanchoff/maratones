/*
*  problem 1006 - Hex-a-bonacci
*  ivanchoff@gmail.com
*
*/

#include <iostream>

using namespace std;

long long a, b, c, d, e, f,res;

int main() {
    int n, caseno = 0, cases;
    cin>>cases;
    while( cases-- ) {
		cin>>a;cin>>b;cin>>c;cin>>d;cin>>e;cin>>f;cin>>n;
		for(int i=6;i<=n;i++){
			res = (a+b+c+d+e+f)%10000007;
			a=b;b=c;c=d;
			d=e;e=f;
			f=res;
		}
		if(n==0)f=a;
		if(n==1)f=b;
		if(n==2)f=c;
		if(n==3)f=d;
		if(n==4)f=e;
		if(n==5)f=f;        
		cout<<"Case "<<++caseno<<": "<<f%10000007<<endl;
	}
    return 0;
}
