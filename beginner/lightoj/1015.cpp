/*
* 1015 - Brush (I)
* ivanchoff@gmail.com
*
*/

#include<iostream>
#include<cmath>

using namespace std;
int main(){
	int tc,res,numStudents,x,i=0;
	cin>>tc;

	while(tc--){
		res=0;
		cin>>numStudents;
		while(numStudents--){
			cin>>x;
			if(x>0)res+=x;
		}
		cout<<"Case "<<++i<<": "<<res<<endl;
	}
return 0;
}
