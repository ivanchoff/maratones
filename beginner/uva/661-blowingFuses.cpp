// uva 661-Blowing Fuses

#include<iostream>
#include<vector>
#include<string>
#include<map>

using namespace std;

int main(){
  vector<pair<int,int> > aparatos;
  int cont,max,cont2=0;
  int n,m,c,tmp;
  
  while(1){
    cin>>n>>m>>c;
    aparatos.clear();
    cont=0;
    max=0;
    ++cont2;
    if(n==m && m==c && c==0) break;
    while(n--){
      cin>>tmp;
      aparatos.push_back(make_pair(tmp,0));
    }
    while(m--){
      cin>>tmp;
      tmp--;
      if(aparatos[tmp].second==0){
	cont+=aparatos[tmp].first;
	if(cont>max)max=cont;
	aparatos[tmp].second=1;
      }else{
	cont-=aparatos[tmp].first;
	aparatos[tmp].second=0;
      }
    }
    if(max<=c){
      cout<<"Sequence "<<cont2<<endl<<"Fuse was not blown."<<endl;
      cout<<"Maximal power consumption was "<<max<<" amperes."<<endl<<endl;
    }else
      cout<<"Sequence "<<cont2<<endl<<"Fuse was blown."<<endl<<endl;
  }

  return 0;
}
